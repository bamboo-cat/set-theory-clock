package com.paic.arch.interviews;

/**
 * @author xjiafei
 * ST时钟灯（红灯）
 */
public class STClockLightRed extends STClockLight  {
	public String getValue(){
		//灯亮 则显示“R”
		if(isOn()){
			return "R";
		}
		return super.getValue();
	}
}
