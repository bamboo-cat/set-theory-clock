package com.paic.arch.interviews;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author xjiafei
 * ST时钟时间
 */
public class STClockTime {
	private static final Logger log =LoggerFactory.getLogger(STClockTime.class);
	private int hour;	//小时
	private int min;	//分钟
	private int secd;	//秒
	private final String seperator=":";
	private final String regPattern="[0-2][0-4]"+seperator+"[0-5][0-9]"+seperator+"[0-5][0-9]";	//正则校验时间格式(00:00:00)
	
	/**
	 * @param aTime
	 * @throws Exception
	 */
	public STClockTime(String aTime) throws Exception{
		//时间格式检验不通过则抛出异常
		if(!checkTime(aTime)){
			String errmsg="time format invalid";
			log.error(errmsg);
			throw new Exception(errmsg);
		}
		//初始化时间
		String[] tarr=aTime.split(seperator);
		 hour=Integer.valueOf(tarr[0]);
		 min=Integer.valueOf(tarr[1]);
		 secd=Integer.valueOf(tarr[2]);
	}
	/**
	 * 校验时间格式 同时初始化时间
	 * @param aTime
	 * @return
	 * @throws Exception
	 */
	private boolean checkTime(String aTime) throws Exception{
		Matcher matcher=Pattern.compile(regPattern).matcher(aTime);
		return matcher.find();
	}
	public int getHour() {
		return hour;
	}
	public int getMin() {
		return min;
	}
	public int getSecd() {
		return secd;
	}
	
	
}
