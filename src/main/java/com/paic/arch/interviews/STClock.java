package com.paic.arch.interviews;

/**
 * @author xjiafei
 * ST时钟
 * 		属性：ST时钟时间、ST时钟灯
 * 		行为（服务)：构造、显示
 * 		行为（内部）：解析时间到时钟灯
 * 				    构建时钟灯显示
 */
public class STClock {
	private STClockTime clockTime;	//ST时钟时间
	private STClockLight top=new STClockLightYellow();	//ST时钟灯顶部
	private STClockLight[] fhours=new STClockLight[]{new STClockLightRed(),new STClockLightRed(),new STClockLightRed(),new STClockLightRed()};	//ST时钟灯第二行（5小时灯）
	private STClockLight[] shours=new STClockLight[]{new STClockLightRed(),new STClockLightRed(),new STClockLightRed(),new STClockLightRed()};	//ST时钟灯第三行（1小时灯）
	private STClockLight[] ffmins=new STClockLight[]{new STClockLightYellow(),new STClockLightYellow(),new STClockLightRed(),
			new STClockLightYellow(),new STClockLightYellow(),new STClockLightRed(),
			new STClockLightYellow(),new STClockLightYellow(),new STClockLightRed(),
			new STClockLightYellow(),new STClockLightYellow()};	//ST时钟灯第四行（5分钟灯） YYRYYRYYRYY
	private STClockLight[] smins=new STClockLight[]{new STClockLightYellow(),new STClockLightYellow(),new STClockLightYellow(),new STClockLightYellow()};	//ST时钟灯第五行（1分钟灯）
	
	/**
	 * 根据时间构造ST时钟
	 * @param aTime	时间
	 * @throws Exception  
	 */
	public STClock(String aTime) throws Exception{
		this.clockTime=new STClockTime(aTime);
		parseTime();
	}
	
	/**
	 * 解析ST时钟时间（调用ST时钟时间解析器）
	 * 
	 */
	protected void parseTime(){
		new TimeParser().parse();
	}
	/**
	 * 构建ST时钟灯（调用ST时钟等构建器）
	 * @return
	 */
	protected String buildTime(){
		return new TimeBuiler().build();
	}
	/**
	 * 显示ST时钟时间
	 * @return
	 */
	public String showTime(){
		return buildTime();
	}
	/**
	 * ST时钟时间解析器
	 */
	private class TimeParser{
		StringBuffer sbTime=new StringBuffer();
		/**
		 * 解析顶部秒灯
		 * @return
		 */
		private TimeParser parseTop(){
			//秒针显示偶数则灯亮
			top.setOnff(clockTime.getSecd()%2==0?1:0);
			return this;
		}
		/**
		 * 解析小时灯
		 * @return
		 */
		private TimeParser parseHour(){
			/**
			 * 第二排灯为5小时显示一个灯，则亮灯的数量为：小时数除以5整数
			 * 第三排灯为1小时显示一个灯，则亮灯的数量为:小时数除以5余数
			 */
			int whole=clockTime.getHour()/5;
			int left=clockTime.getHour()%5;
			for(int i=0;i<whole;i++){
				fhours[i].setOnff(1);
			}
			for(int i=0;i<left;i++){
				shours[i].setOnff(1);
			}
			return this;
		}
		/**
		 * 解析分钟灯
		 * @return
		 */
		private TimeParser parseMin(){
			/**
			 * 第四排灯为5分钟显示一个灯，则亮灯的数量为：分钟数除以5整数
			 * 第五排灯为1分钟显示一个灯，则亮灯的数量为:分钟数除以5余数
			 */
			int whole=clockTime.getMin()/5;
			int left=clockTime.getMin()%5;
			for(int i=0;i<whole;i++){
				ffmins[i].setOnff(1);
			}
			for(int i=0;i<left;i++){
				smins[i].setOnff(1);
			}
			return this;
		}
		/**
		 * 整体解析方法
		 */
		public void parse(){
			parseTop().parseHour().parseMin();
		}
	}
	
	/**
	 * ST时钟灯构建器
	 */
	private class TimeBuiler{
		private final String lineBreak= "\r\n";
		StringBuffer sbTime=new StringBuffer();
		/**
		 * 构建顶部秒灯显示
		 * @return
		 */
		private TimeBuiler buildTop(){
			sbTime.append(top.getValue()).append(lineBreak);
			return this;
		}
		/**
		 * 构建小时灯显示
		 * @return
		 */
		private TimeBuiler buildHour(){
			//构建5小时灯和1小时灯显示
			for(STClockLight light:fhours){
				sbTime.append(light.getValue());
			}
			sbTime.append(lineBreak);
			for(STClockLight light:shours){
				sbTime.append(light.getValue());
			}
			sbTime.append(lineBreak);
			return this;
		}
		/**
		 * 构建分钟灯显示
		 * @return
		 */
		private TimeBuiler buildMin(){
			//构建5分钟灯和1分钟灯显示
			for(STClockLight light:ffmins){
				sbTime.append(light.getValue());
			}
			sbTime.append(lineBreak);
			for(STClockLight light:smins){
				sbTime.append(light.getValue());
			}
			return this;
		}
		/**
		 * 整天构建方法
		 * @return
		 */
		public String build(){
			buildTop().buildHour().buildMin();
			return sbTime.toString();
		}
	}
}
