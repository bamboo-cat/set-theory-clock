package com.paic.arch.interviews;

/**
 * @author xjiafei
 * ST时钟灯（黄灯）
 */
public class STClockLightYellow extends STClockLight  {
	public String getValue(){
		//灯亮 则显示“Y”
		if(isOn()){
			return "Y";
		}
		return super.getValue();
	}
}

