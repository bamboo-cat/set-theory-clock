package com.paic.arch.interviews;

/**
 * @author xjiafei
 * ST时钟灯
 */
public class STClockLight { 
	private int onff;	//时钟灯开关	开:1   关:0
	 
	public void setOnff(int onff) {
		this.onff = onff;
	}

	/**
	 * 是否是开
	 * @return
	 */
	protected boolean isOn(){
		return onff==1;
	}
	
	/**
	 * 灯开关显示值 
	 * @return
	 */
	public String getValue(){
		//默认显示灯灭的值
		return "O";
	}
}
