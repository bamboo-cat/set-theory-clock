package com.paic.arch.interviews.impl;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.paic.arch.interviews.STClock;
import com.paic.arch.interviews.TimeConverter;

/**
 * @author xjiafei
 * 分析(业务流程)：	1.校验参数（时间格式:00:00:00)
 * 				2.解析时间(00:00:00)--时、分、秒
 * 				3.转换时间（灯）（转换时、转换分）
 * 				4.显示时间（灯）
 * 设计（面向对象）：	1.对象：ST时钟、ST时钟灯（红灯和黄灯）、ST时间
 * 				2.对象关系：见类关系图
 * 				3.对象属性和行为：见类图	
 * 
 * ST时钟时间转换实现类			
 */
public class STClockTimeConverter implements TimeConverter {
	private static final Logger log =LoggerFactory.getLogger(STClockTimeConverter.class);

	@Override
	public String convertTime(String aTime) {
		try{
			//构造ST时钟对象展示时间
			return new STClock(aTime).showTime();
		}catch(Exception e){
			log.error(e.getMessage(),e);
		}
		return "";
	}

}
